'use strict'

// setup
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const User = require('../models/user')

// helpers

const generateJWT = async (payload) => {
  return jwt.sign(
    { user: payload }, // payload
    process.env.JWT_SECRET, // secret
    { expiresIn: '7d' } // options
  )
}

// route handlers

const login = async (req, res, next) => {
  const email = req.body.email
  const password = req.body.password

  // 1. find user by email address in the database
  const user = await User.findByEmail(email)
  if (!user) {
    return res.status(404).json({ message: 'no user with that email' })
  }

  // 2. check password (hash and compare)
  const valid = await bcrypt.compare(password, user.passwordHash)
  if (!valid) {
    return res.status(400).json({ message: 'invalid login creds' })
  }

  // 3. generate a JWT and return to client
  return generateJWT({ _id: user._id, email: user.email })
    .then(token => res.status(200).json({ user: user.email, token }))
    .catch(err => next(err))
}

/**
 * We expect email and password in request body
*/
const register = async (req, res, next) => {
  // 1. check if user already registered. How. Are they in the database
  const user = await User.exists(req.body.email)
  if (user) {
    return res.status(500).json({ message: 'email already taken' })
  }

  // 2. if user NOT in db, create new user
  const newUser = {
    email: req.body.email
  }

  // 3. hash pw
  const salt = await bcrypt.genSalt(10)
  newUser.passwordHash = await bcrypt.hash(req.body.password, salt)

  // 4. store in db
  // 5. generate a JWT and return to client
  return User.create(newUser)
    .then(({ email, _id }) => generateJWT({ email, _id }))
    .then(token => res.status(200).json({ token }))
    .catch(err => next(err))
}

// exports

exports = module.exports = { register, login }
