# peet-auth

TODO

1. write jwt-checking middleware. To be used on API routes to verify requests have valid JWT tokens, and parse token payload properties, setting them on a `req.user` object. This will enable controllers to query DB for that user's own resources. We'll especially need to parse out the user's `_id` for later use in the controllers and models.
2. write Note model: `/models/note.js`. **Think about note schema.** Title, body, _author_, timestamps, etc.
3. wire up Note API controller: `/controllers/note.js`. **Think about parsing request properties.** Get user's `_id` from the `req.user` object created by our JWT checking middleware.
4. write catch-all error handling middleware in main `index.js` after routes, to catch all the calls to `next(err)` from the auth and api routes.
