'use strict'

const router = require('express').Router()
const notesCtrl = require('../controllers/notes')
const validateJwtMiddleware = require('../middleware/validateJwtMiddleware')

router.get('/health', (req, res) => {
  res.status(200).json({ host: 'http://localhost:3000', status: 'OK' })
})

router.use(validateJwtMiddleware)

router.route('/notes')
  .get(notesCtrl.getAll)
  .post(notesCtrl.create)

router.route('/notes/:id')
  .get(notesCtrl.getOne)
  .put(notesCtrl.update)
  .delete(notesCtrl.deleteNote)

exports = module.exports = router
