const jwt = require('jsonwebtoken')

/**
 * Expects that req.headers includes an 'authorization' property.
 * Decodes the value of that property to obtain user's _id and email.
 */
exports = module.exports = (req, res, next) => {
  const secret = process.env.JWT_SECRET
  const authHeader = req.headers.authorization
  const headerRegex = /^Bearer\s{1}[\w\-.]+$/

  if (!authHeader) {
    return next(new Error('Missing Authorization header'))
  }

  if (!headerRegex.test(authHeader)) {
    return next(new Error('Malformed Authorization header'))
  }

  const token = authHeader.split(' ')[1]

  /**
   * Decode the token
   * @param  {String}    token    Our JWT token
   * @param  {String}    secret   Our JWT enciphering secret
   * @param  {Function}  <anon>   Callback to deal with results
   */
  jwt.verify(token, secret, (err, decoded) => {
    if (err) {
      return next(new Error(err.message))
    }

    if (decoded.user._id && decoded.user.email) {
      req.user = decoded.user
      next()
    } else {
      return next(new Error('Malformed JWT payload'))
    }
  })
}
