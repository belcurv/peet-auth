'use strict'

const bunyan = require('bunyan')
const uuidV4 = require('uuid/v4')

exports = module.exports = () => (req, res, next) => {
  req.log = bunyan.createLogger({
    name: 'Note App',
    requestId: uuidV4(),
    serializers: bunyan.stdSerializers
  })
  next()
}
