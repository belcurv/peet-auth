'use strict'

require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const db = require('./db')
const { requestMiddleware, loggerMiddleware, errorMiddleware } = require('./middleware')

const app = express()
const port = process.env.PORT || 3000

app.use(loggerMiddleware())
app.use(requestMiddleware())
app.use(cors())
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

// routes
app.use('/auth', require('./routes/auth_routes'))
app.use('/api', require('./routes/api_routes'))

// catch-all error handler
app.use(errorMiddleware())

// connect to db
db.connect((err) => {
  if (err) {
    console.log('Unable to connect to mongo, suicide.')
    process.exit(1)
  }

  app.listen(port, () => console.log(`Listening on port ${port}`))
})
