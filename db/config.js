require('dotenv').config()

module.exports = {

  development: {
    url: `mongodb://${process.env.DB_URL}:${process.env.DB_PORT}`,
    dbName: process.env.DB_NAME
  }

}
