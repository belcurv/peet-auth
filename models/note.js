'use strict'

const ObjectID = require('mongodb').ObjectID
const db = require('../db')

const getAll = ({ authorId }) => {
  const collection = db.get().collection('notes')
  return collection
    .find({ authorId })
    .toArray()
}

const create = (note) => {
  const collection = db.get().collection('notes')
  const now = Date.now()
  note.createdAt = now
  note.updatedAt = now
  return collection.insertOne(note)
    .then(result => result.ops[0])
}

const getOne = ({ noteId, authorId }) => {
  const collection = db.get().collection('notes')
  return collection.findOne({
    _id: ObjectID(noteId),
    authorId: authorId
  })
}

// @TODO: everything below this line

const update = (id, update) => {
  const collection = db.get().collection('notes')
  return collection.findOneAndUpdate({ _id: id }, update)
}

const deleteNote = (id) => {
  const collection = db.get().collection('notes')
  return collection.findOneAndDelete({ _id: id })
}

exports = module.exports = {
  getAll, create, getOne, update, deleteNote
}
