'use strict'

const { STATUS_CODES } = require('http')

exports = module.exports = () => (req, res, next) => {
  req.log.info(`Received ${req.method.toUpperCase()} request for: ${req.url}`)

  // listen for 'finish' events
  res.on('finish', () => {
    const statusMessage = STATUS_CODES[res.statusCode]
    const level = res.statusCode < 400 ? 'info' : 'error'
    req.log[level](`${res.statusCode} ${statusMessage}; Resolved ${req.method.toUpperCase()} with ${res.get('Content-Length') || 0}b`)
  })
  next()
}
