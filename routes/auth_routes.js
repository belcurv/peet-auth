'use strict'

const router = require('express').Router()
const authCtrl = require('../controllers/auth')

router.post('/register', authCtrl.register)
router.post('/login', authCtrl.login)

exports = module.exports = router
