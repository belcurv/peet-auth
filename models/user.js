'use strict'

const db = require('../db')

const findByEmail = (email) => {
  const collection = db.get().collection('user')
  return collection.findOne({ email })
}

const exists = (email) => {
  const collection = db.get().collection('user')
  return collection.find({ email })
    .count()
    .then(count => count > 0)
}

const create = ({ email, passwordHash }) => {
  const collection = db.get().collection('user')
  const newUser = {
    email,
    passwordHash,
    createdAt: Date.now(),
    updatedAt: Date.now()
  }
  return collection.insertOne(newUser)
    .then(result => result.ops[0])
}

exports = module.exports = { findByEmail, exists, create }
