'use strict'

const Note = require('../models/note')

// Get all of a user's own notes
// Example: GET >> /api/notes
// Secured: true
// Expects:
//   1) note author's ID from req.user._id object
// Returns: JSON array of a user's own notes
const getAll = (req, res, next) => {
  return Note.getAll({ authorId: req.user._id })
    .then(notes => res.status(200).json(notes))
    .catch(err => next(err))
}

// Create a new note
// Example: POST >> /api/notes
// Secured: true
// Expects:
//   1) note author's ID from req.user._id object
//   2) note content from req.body object
// Returns: JSON; newly created note
const create = (req, res, next) => {
  const newNote = {
    authorId: req.user._id,
    title: req.body.title,
    content: req.body.content
  }
  return Note.create(newNote)
    .then(note => res.status(200).json(note))
    .catch(err => next(err))
}

// Get a single author's Note
// Example: GET >> /api/notes/abcfwer
// Secured: true
// Expects:
//   1) note `id` from req.params object
//   2) note author's `_id` from req.user object
// Returns: JSON; single note
const getOne = (req, res, next) => {
  return Note.getOne({ noteId: req.params.id, authorId: req.user._id })
    .then(note => note == null
      ? res.status(404).end()
      : res.status(200).json(note)
    )
    .catch(err => next(err))
}

// Example: PUT >> /api/notes/abcfwer
// Secured: true
// Expects:
//   1) note `id` from req.params object
//   2) note author's `_id` from req.user object
//   3) updated content from req.body object
// Returns: JSON; updated note
const update = async (req, res, next) => {
  return Note.update()
}

// Example: DELETE >> /api/notes/abcfwer
// Secured: true
// Expects:
//   1) note `id` from req.params object
//   2) note author's `_id` from req.user object
// Returns: JSON; deleted note
const deleteNote = async (req, res, next) => {
  return Note.deleteNote()
}

exports = module.exports = {
  getAll, create, getOne, update, deleteNote
}
